#! /usr/bin/env node

//[1]shebang line

//=>so somehow this line is needed for the './store.js or 'store' (i think the last one works on windows only)

//[2]then you need to run this `chmod +x store.js` in console

//[3] adding "bin" : {"store" : "./store.js"} ***already added***

//[4]finally run `npm link`

const fs = require('fs');
const database = "./dictionary.json";
var key;
var value;
var dictionary = {};



if (process.argv[2] !== undefined) { //if there is a command

    let commandArgument = process.argv[2];

    switch (commandArgument) {


        //-----[case start ] ---------//
        case 'add':

            if (process.argv.length !== 5)
                console.log('You shall not pass ... re-check your syntax');
            else {

                let keyArgument = process.argv[3];
                let valueArgument = process.argv[4];

                fs.readFile(database, (err, data) => {
                    if (err) throw err;

                    else {
                        dictionary = JSON.parse(data.toString());
                        if (Object.keys(dictionary).includes(keyArgument)) {
                            console.log('Key already Exists');
                        } else {
                            dictionary[keyArgument] = valueArgument;
                            fs.writeFileSync(database, JSON.stringify(dictionary, null, 2));
                            // second argument is for replacer [A function ] -- not needed
                            // third argument is how much space in indents
                            console.log('Success! - key and value are added.');
                        }
                    }


                });
            }

            break;
            //-----[case end ] ---------//



            //-----[case start ] ---------//
        case 'list':
            fs.readFile(database, (err, data) => {
                if (err) throw err;
                else {
                    if (data.toString() === '{}') {
                        console.log('Your dictionary is empty dear.');
                    } else {
                        console.log(data.toString());
                    }
                }


            });
            break;
            //-----[case end ] ---------//




            //-----[case start ] ---------//
        case 'get':
            if (process.argv.length !== 4) {
                console.log('You shall not pass ... re-check your syntax');
            } else {
                let keyArgument = process.argv[3];
                fs.readFile(database, (err, data) => {
                    if (err) throw err;
                    else {
                        dictionary = JSON.parse(data.toString());
                        if (Object.keys(dictionary).includes(keyArgument)) {
                            console.log('["' + keyArgument + '"] => ["' + dictionary[keyArgument] + '"]');
                        } else {
                            console.log('Error this->key doesn\'t exist');
                        }
                    }


                });
            }

            break;
            //-----[case end ] ---------//



            //-----[case start ] ---------//
        case 'remove':
            if (process.argv.length !== 4) {
                console.log('You shall not pass ... re-check your syntax');
            } else {
                let keyArgument = process.argv[3];
                fs.readFile(database, (err, data) => {
                    if (err) throw err;

                    else {
                        dictionary = JSON.parse(data.toString());
                        if (Object.keys(dictionary).includes(keyArgument)) {
                            delete dictionary[keyArgument];
                            fs.writeFileSync(database, JSON.stringify(dictionary, null, 2));
                            console.log('Success! - key removed.');
                        } else {
                            console.log('Error key doesn\'t exist');
                        }
                    }


                });
            }

            break;
            //-----[case end ] ---------//



            //-----[case start ] ---------//
        case 'clear':
            fs.exists(database, (err) => {
                if (err) throw err;
                else {
                    fs.writeFileSync(database, JSON.stringify(dictionary, null, 2));
                    console.log('Success! - Cleared.');

                }

            });
            break;
            //-----[case end ] ---------//



        default:
            console.log('Your arguments are vague and unconvincing.');
            break;
    }

} else {
    console.log('Can a relationship survive without an argument?');
}